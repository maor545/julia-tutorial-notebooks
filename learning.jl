### A Pluto.jl notebook ###
# v0.19.9

using Markdown
using InteractiveUtils

# ╔═╡ 86f08ec0-4108-11ee-07b7-dfa617a9d0e5
#type of var
typeof("a")

# ╔═╡ a3b69ab7-fe4a-4b26-834c-e68f5652190e
begin
a=64
typeof(a)
end

# ╔═╡ ee2d9bc8-af4c-401f-a9fa-79833d344136
#? gets help and examples
? println

# ╔═╡ 4399d011-7bd6-4ed4-9929-4c20abb4791b
begin
a1, b1 = a, 20
end

# ╔═╡ af8afa43-e696-4cde-89a9-ca34f62be455
# literal type - what is after : is the value
:best

# ╔═╡ 377c3267-83a1-4810-9fbd-3bebbe4255a8
:4

# ╔═╡ 32ae546e-48b9-4f76-87c8-ff040e42f813
#list uses type promotion - all vars in the list is type float because we have one float
println([3+1, 3/4])

# ╔═╡ d7e448b6-15c6-47e2-8078-b5a3adc4e880
#
print(3+1,3/4)

# ╔═╡ 64946f82-a8c6-43d2-b455-1fe982de5bdb
# rationals
4//2

# ╔═╡ 38e5ab47-2a6a-4595-ad06-c6727e85be96
convert(Float64, 1)

# ╔═╡ 3f1c1172-bb41-421e-8536-363797d05df0
yes = true

# ╔═╡ 55e902e6-d6c9-4bc4-9a69-e6b6d62d2587
!yes

# ╔═╡ d22a1445-6c85-4745-b1d7-04b74313d9f6
print("Ahoj $(yes)")

# ╔═╡ 257d58ad-e73c-4657-9b63-d20ebe654eb8
begin
x1 = "Ahoj"
	x2 = "Pepa"
	lstr = x1 * x2 # join two strings is with *
end

# ╔═╡ 54c6dd81-7fd9-44d5-b9a8-5fba9c58e576
#string repetition
x1 ^ 3

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.9.2"
manifest_format = "2.0"
project_hash = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

[deps]
"""

# ╔═╡ Cell order:
# ╠═86f08ec0-4108-11ee-07b7-dfa617a9d0e5
# ╠═a3b69ab7-fe4a-4b26-834c-e68f5652190e
# ╠═ee2d9bc8-af4c-401f-a9fa-79833d344136
# ╠═4399d011-7bd6-4ed4-9929-4c20abb4791b
# ╠═af8afa43-e696-4cde-89a9-ca34f62be455
# ╠═377c3267-83a1-4810-9fbd-3bebbe4255a8
# ╠═32ae546e-48b9-4f76-87c8-ff040e42f813
# ╠═d7e448b6-15c6-47e2-8078-b5a3adc4e880
# ╠═64946f82-a8c6-43d2-b455-1fe982de5bdb
# ╠═38e5ab47-2a6a-4595-ad06-c6727e85be96
# ╠═3f1c1172-bb41-421e-8536-363797d05df0
# ╠═55e902e6-d6c9-4bc4-9a69-e6b6d62d2587
# ╠═d22a1445-6c85-4745-b1d7-04b74313d9f6
# ╠═257d58ad-e73c-4657-9b63-d20ebe654eb8
# ╠═54c6dd81-7fd9-44d5-b9a8-5fba9c58e576
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
